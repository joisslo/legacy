//
//  DataManager.swift
//  CollegeIOS
//
//  Created by Jerry Hill on 2/19/17.
//  Copyright © 2017 Jerry Hill. All rights reserved.
//

import Foundation
import UIKit

private let _storeInstance = Store()

class Store : NSObject
{
    class func storeInstance() -> Store
    {
        return _storeInstance
    }
    
    var student = DTO_Student()
    var students = [DTO_Student]()
    var campuses = [DTO_Campus]()
}

class DataManager
{
    func loadAllData(completionClosure:@escaping () ->())
    {
        self.loadAllCampuses()
            {
                () in
                OperationQueue.main.addOperation
                    {
                        self.printCampuses()
                        completionClosure()
                }
                
        }
    }
    func loadAllStudents(completionClosure:@escaping () ->())
    {
        BLL.post( params: [:], remoteMethod: "GetStudents")
        {
            ( data: NSData) in
            
            OperationQueue.main.addOperation
                {
                    BLL.Extract_DTO_Student(responseData: data)
                    completionClosure()
            }
        }
    }
    
    func loadAllCampuses(completionClosure:@escaping () ->())
    {
        BLL.post( params: [:], remoteMethod: "GetCampuses")
        {
            ( data: NSData) in
            
            OperationQueue.main.addOperation
                {
                    BLL.Extract_DTO_Campus(responseData: data)
                    completionClosure()
            }
        }
    }
    func printStudents()
    {
        print("*** Students ***")
        for student in Store.storeInstance().students
        {
            print( "Student ID: \(student.StudentID) Name: \(student.StudentName)")
            
        }
    }
    func printCampuses()
    {
        print("*** Campuses ***")
        for campus in Store.storeInstance().campuses
        {
            print( "Campus ID: \(campus.CampusName) Name: \(campus.CampusID)")
            
        }
    }
    func uploadImage(studentID: Int, image: String, fileName: String , imageData: String, completionClosure: @escaping () ->())
    {
        let jsonrequest = [ "studentID" : String(studentID),
                            "FileName" : fileName,
                            "Image" : image,
                            "ImageData" : imageData]
                            
        
        //print(jsonrequest)
        
        BLL.post(params: jsonrequest, remoteMethod: "uploadImage") {
            
            (data: NSData) in
            
            OperationQueue.main.addOperation {
                
                BLL.Extract_DTO_Image(responseData: data)
                completionClosure()
            }
        }
    }

}

class BLL
{
    static let BaseURL : String = "http://www.jumpcreek.com/collegefall/Service1.svc/"
    
    static let BaseImageURL : String = "http://www.jumpcreek.com/Collegeimages/"
    static var ErrorMessage : String = ""
    
    
    static func post(params : Dictionary<String, String>, remoteMethod: String ,completionClosure: @escaping (_ data :NSData) ->())
    {
        let urlString: String = "\(BaseURL)\(remoteMethod)"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        do
        {
            
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue( "application/json", forHTTPHeaderField: "Accept")
        }
        catch
        {
            print("Error:\n \(error)")
            ErrorMessage = "\(error)"
            return
        }
        
        
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            guard let data = data, error == nil else
            {
                print("General Network error=\(error)")
                return
            }
            
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            completionClosure(data as NSData)
            
        }
        task.resume()
    }
    static func Extract_DTO_Student(responseData: NSData)
    {
        if( responseData.length == 0)
        {
            return
        }
        
        Store.storeInstance().students.removeAll()
        
        do
        {
            let json = try JSONSerialization.jsonObject(with: responseData as Data, options: .allowFragments)
                as! [String:AnyObject]
            
            print(json)
            
            
            if let items = json["Data"] as? [[String:AnyObject]]
            {
                for item in items
                {
                    let single = DTO_Student.Create(dict: item as NSDictionary )
                    
                    Store.storeInstance().students.append(single)
                    Store.storeInstance().student = single
                    
                }
            }
        }
        catch let error
        {
            print("error parsing DTO_Student \(error)")
            return
        }
        
        return
    }
    
    static func Extract_DTO_Campus(responseData: NSData)
    {
        if( responseData.length == 0)
        {
            return
        }
        
        Store.storeInstance().campuses.removeAll()
        
        do
        {
            let json = try JSONSerialization.jsonObject(with: responseData as Data, options: .allowFragments)
                as! [String:AnyObject]
            
            print(json)
            
            
            if let items = json["Data"] as? [[String:AnyObject]]
            {
                for item in items
                {
                    let single = DTO_Campus.Create(dict: item as NSDictionary )
                    
                    Store.storeInstance().campuses.append(single)
                   
                    
                }
            }
        }
        catch let error
        {
            print("error parsing DTO_Campus \(error)")
            return
        }
        
        return
    }
    static func Extract_DTO_Image(responseData: NSData)
    {
        if( responseData.length == 0)
        {
            return
        }
        
        do
        {
            let json = try JSONSerialization.jsonObject(with: responseData as Data, options: .allowFragments)
                as (AnyObject)
            
            print(json)
            
            if let item = json["Data"] as? [String:AnyObject] {
                
                let single = DTO_Image.Create(dict: item as NSDictionary)
                
                //Store.storeInstance().image = single
            }
        }
        catch let error
        {
            print("error parsing DTO_Image \(error)")
            return
        }
        
        return
    }
 
}


class DTO
{
    
    static func parse(dict: NSDictionary, key: String)->String
    {
        if let i = dict.value(forKey: key ) as? String
        {
            return i
        }
        else
        {
            return ""
        }
    }
    
    static func parse(dict: NSDictionary, key: String)->Int
    {
        if let i = dict.value(forKey: key ) as? Int
        {
            return i
        }
        else
        {
            return 0
        }
    }
    static func parse(dict: NSDictionary, key: String)->Double
    {
        if let i = dict.value(forKey: key ) as? Double
        {
            return i
        }
        else
        {
            return 0
        }
    }
    
}


class DTO_Campus : DTO
{
    var CampusID : Int = 0
    var CampusName = ""
    
    static func Create(dict: NSDictionary) ->DTO_Campus
    {
        
        let dto = DTO_Campus()
        
        dto.CampusID = parse(dict: dict, key: "CampusID")
        dto.CampusName = parse(dict: dict, key: "CampusName")
        
        return dto
        
        
    }
}

class DTO_Student : DTO
{
    var StudentID : Int = 0
    var StudentName = ""
    var pic = ""
    
    static func Create(dict: NSDictionary) ->DTO_Student
    {
        
        let dto = DTO_Student()
        
        dto.StudentID = parse(dict: dict, key: "StudentID")
        dto.StudentName = parse(dict: dict, key: "StudentName")
        dto.pic = parse(dict: dict, key: "pic")
        
        return dto
        
        
    }
}

class DTO_Image : DTO {
    
    var imageURL = ""
    
    static func Create(dict: NSDictionary) ->DTO_Image {
        
        let dto = DTO_Image()
        
        dto.imageURL = parse(dict: dict, key: "url")
        
        return dto
    }
}

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


